express = require 'express'
jade = require 'jade'
knox = require 'knox'
randomstring = require 'randomstring'
coffeescript = require 'connect-coffee-script'

config = {}
secret = {}

try
	config = require './config'
	secret = require './secret'

app = express()

cors = (req, res, next) ->
	res.header('Access-Control-Allow-Origin', '*')
	res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS')
	res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With, X-File-Name')

	if req.method == 'OPTIONS'
		res.send(200)
	else
		next()

app.configure () ->
	app.set('port', process.env.PORT || config.PORT || 5000)

	app.set('views', __dirname + '/views')
	app.set('view engine', 'jade')

	app.use(express.logger('dev'))
	app.use(express.favicon())

	app.use(cors)

	app.use '/v0', coffeescript({
		src: __dirname + '/public',
		dest: __dirname + '/public',
		pretty: true
	})

	app.use '/v0', express.static(__dirname + '/public')

	app.use(express.bodyParser())
	app.use(express.methodOverride())

	app.use(app.router)

app.get '/i/:filename', (req, res, next) ->
	s3 = knox.createClient({
		key: process.env.AWS_ACCESS_KEY_ID || secret.AWS_ACCESS_KEY_ID,
		secret: process.env.AWS_SECRET_ACCESS_KEY || secret.AWS_SECRET_ACCESS_KEY,
		bucket: process.env.S3_BUCKET_NAME || config.S3_BUCKET_NAME,
		region: config.S3_REGION
	})

	filename = req.params.filename

	s3.getFile filename, (err, s3res) ->
		# err check

		res.status(200)
		res.type(s3res.headers['content-type'])

		s3res.resume()

		s3res.on 'data', (chunk) ->
			res.write(chunk)

		s3res.on 'end', () ->
			res.end()

app.post '/upload', (req, res, next) ->
	res.type('json')

	image = req.files.image

	if !image
		res.send(200, {
			error: 'ERROR_NO_UPLOAD_FILE',
		})
	else
		s3 = knox.createClient({
			key: process.env.AWS_ACCESS_KEY_ID || secret.AWS_ACCESS_KEY_ID,
			secret: process.env.AWS_SECRET_ACCESS_KEY || secret.AWS_SECRET_ACCESS_KEY,
			bucket: process.env.S3_BUCKET_NAME || config.S3_BUCKET_NAME,
			region: config.S3_REGION
		})

		headers = {
			'Content-Type': image.type,
			'x-amz-acl': 'public-read'
		}

		ext = image.name.split('.').pop()
		identifier = randomstring.generate(8)
		filename = identifier + '.' + ext

		s3.putFile image.path, filename, headers, (err, s3res) ->
			# err check
			s3res.resume()

			res.send(200, {
				response: filename
			})

app.all '/error/:error/upload', (req, res, next) ->
	res.type('json')

	switch req.params.error.toLowerCase()
		when 'timeout_client'
			setTimeout () ->
				res.send(200, { error: 'TIMEOUT' })
			, 10 * 1000

		when 'timeout_server'
			setTimeout () ->
				res.send(200, { error: 'TIMEOUT' })
			, 1 * 1000

		when 'abort'
			setTimeout () ->
				res.send(200, { error: 'ABORT' })
			, 5 * 1000

		when 'statuscode'
			res.send(400, {})

		else
			res.send(400, { error: 'You must provide an error specification' })


app.get '/*', (req, res, next) ->
	res.send(200, 'Server Running')

app.listen app.get('port'), () ->
	console.log "express listening on %d", app.get('port')

	